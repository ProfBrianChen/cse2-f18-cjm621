/////
// Chris Mann
// CSE 02
// Due: 9/4/18
/////
// CSE 02 Welcome Class
/////
public class WelcomeClass{
  
  public static void main(String args[]){
    System.out.println("  -----------");
    System.out.println("  |  WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-C--J--M--6--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("My name is Chris Mann, I'm 20 and live in Bedford Hills, NY.");
  }
}